---
title: "Firefly Algorithm"
date: 2021-07-22
tags: ["firefly algorithm", "nature-inspired optimization", "go", "golang"]
author: "Me"
showToc: true
TocOpen: true
draft: false
hidemeta: false
comments: false
description: "An implementation of the firefly algorithm in Go."
canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: true
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
cover:
    image: "fireflies.jpg" # image path/url
    alt: "Fireflies" # alt text
    caption: "Image [@phanchutoan](https://unsplash.com/@phanchutoan)" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
editPost:
    URL: "https://gitlab.com/moejoe95/myblog/-/blob/master/content"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---

## Intro

Today we will look into a nature-inspired optimization algorithm, which was proposed by Xin-She Yang [here](https://link.springer.com/chapter/10.1007/978-3-642-04944-6_14). The algorithm is inspired by the flashing behavior of fireflies. We will not go into biological details of this
phenomenon but instead focus on the simplified set of rules that artificial fireflies follow, as presented in the paper. Firstly, any firefly
is attracted by any other firefly. Secondly, the attraction depends on the brightness of the fireflies, and lastly, the attraction decreases with increasing
distance.  Each firefly represents one solution to the optimization problem. We initialize the fireflies randomly over the search space. The brightness of a
firefly is determined by the firefly's error. At each iteration, each firefly moves a little step into the direction of each other firefly that has a higher
brightness.

## The problem

We will use a very simplistic problem here. Suppose we have given a random string of 11 characters and we want to end up at the string *helloworld*. In this simple case, we could also use a brute force algorithm to get from the random string to our desired one, but we will find a way to formulate this as a problem we can solve with the firefly algorithm.

The idea for this toy problem I got from an [opt4j tutorial](https://sdarg.github.io/opt4j/documentation/3.3.0/tutorial.xhtml).

## Prerequisites

At first, we have to define our search space, which is, according to our problem, the alphabet. Furthermore, we need the ground truth of the string we want to get to, because without we wouldn't be able to define a metric on how close we are to this string. 

```go
var alphabet = [...]byte{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'}
var trueChars = [...]byte{'h', 'e', 'l', 'l', 'o', 'w', 'o', 'r', 'l', 'd'}
```

For our algorithm we need two metrics: an error function, to determine the quality of a solution, and, a distance function, describing how far apart are two solutions from each other. Examples of those two functions are given later in the section *Helper functions*.

Next, we need to define how a firefly should look like. One firefly represents one solution to our problem, with an error value attached to it. The intensity of a firefly determines how attractive one firefly seems to other fireflies. In the case of a minimization problem, we can define the intensity as `1/error`.

```go
type Firefly struct {
	positions []byte
	err       int
	intensity float64
}
```

### Initialization

Before we start the iterative optimization procedure, we initialize a population of fireflies with a random string and set the error and intensity to 0.

```go
var fireflies = make([]Firefly, population)
for i := 0; i < population; i++ {
    var positions = make([]byte, dim)
    // give random positions
    for j := 0; j < dim; j++ {
        positions[j] = alphabet[rand.Intn(len(alphabet))]
    }
    fireflies[i] = Firefly{positions: positions, err: 0, intensity: 0.0}
}
```

### The algorithm

At each iteration of the algorithm, we compare each firefly to every other firefly. If the intensity of the current firefly we are looking at is smaller than the intensity of another fly, we move a little bit in the direction of the other firefly. This way each firefly tries to reduce its error.

The actual movement of a firefly is described in the paper as:

$$x_i = x_i + \beta e^{- \gamma r_{ij}^2} + \alpha (rand - \frac{1}{2})$$

where $x_i$ is the current position, $r_{ij}$ is the distance between the two fireflies, and *rand* is a random number between 0 and 1. $\alpha$, $\beta$, and $\gamma$ are hyper-parameters. The position update consists of three major components: the current position, the attraction to the other firefly, and, a random walk term. Given the attraction term, we see that the attraction gets lower as the distance decreases. 

After we updated the position we re-evaluate the current position.

```go
for i := 0; i < population; i++ {
    // for every other firefly
    for j := 0; j < population; j++ {

        if i == j {
            // don't compare firefly with itself
            continue
        }

        // check for greater intensity
        if fireflies[j].intensity >= fireflies[i].intensity {

            dist := distance(fireflies[i].positions, fireflies[j].positions)
            attraction := B0 * math.Exp(-g*dist*dist)
            scale := float64(upperBound - lowerBound)

            // move into direction of higher intensity
            for d := 0; d < dim; d++ {
                singleDist := float64(fireflies[j].positions[d] - fireflies[i].positions[d])
                nextPosFloat := float64(fireflies[i].positions[d]) + attraction*singleDist + a*(rand.Float64()-0.5)*scale

                nextPos := byte(int(nextPosFloat))
                // respawn firefly if we are out of bounds
                if nextPos < byte(lowerBound) || nextPos > byte(upperBound) {
                    nextPos = byte(rand.Intn(upperBound-lowerBound) + lowerBound)
                }

                fireflies[i].positions[d] = nextPos
            }

            // update intensity/error
            fireflies[i].err = errorFunction(fireflies[i].positions)
            fireflies[i].intensity = 1 / float64(fireflies[i].err+1)
        }
    }
}
```

At the end of each iteration we sort the fireflies by their error value and update the current best solution:

```go
sort.Sort(ByError(fireflies))
if fireflies[0].err <= bestError {
    bestError = fireflies[0].err
    bestPosition = fireflies[0].positions
}
```

After updating the best-known position, we restart the procedure above until we hit the iteration counter or we found the global optima, which would be the string *helloworld*.

## Helper functions

As mentioned before, we need to define a distance function and an error function suited for our problem. Intuitively, the error of a firefly should be 0, *iff* the firefly represents the solution *helloworld*. We define our error function to give the number of characters we are off, compared to the ground truth string.

```go
// counts the number characters that are off the true string
func errorFunction(currentChars []byte) int {
	err := 0
	for i := 0; i < len(trueChars); i++ {
		if trueChars[i] != currentChars[i] {
			err++
		}
	}
	return err
}
```

For the distance function, we use the euclidean distance. We define the distance between two characters by the distance between their corresponding ASCII values.

```go
func distance(thisChars []byte, otherChars []byte) float64 {
	err := 0
	for i := 0; i < len(thisChars); i++ {
		truePos := int(thisChars[i])
		currPos := int(otherChars[i])
		if truePos > currPos {
			err = err + (truePos - currPos)
		} else {
			err = err + (currPos - truePos)
		}
	}
	return float64(err)
}
```

## Conclusion

In this post, we saw how we can use the firefly algorithm to solve a simple optimization problem. For more detailed insights I would recommend reading the original paper, which is written in a very clear in easy to follow way.

The full source code is available at this [gist](https://gist.github.com/moejoe95/003799da55653f8b9af08c2003b69ec4). You can run the example code by executing
```sh
go run firfly.go
```


